import HelloWorld from "@/components/HelloWorld";
import { shallowMount } from "@vue/test-utils";

describe("HelloWorld", () => {
  test("ShallowMount test", () => {
    const msg = "Hello";
    const wrapper = shallowMount(HelloWorld, {
        props: {
            msg
        }
    });

    expect(wrapper.find("h1").text()).toBe(msg);
  });
});
